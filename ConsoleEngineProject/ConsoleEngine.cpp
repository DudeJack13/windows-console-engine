#include "stdafx.h"
#include "ConsoleEngine.h"

consoleengine::ConsoleEngine::ConsoleEngine()
{
	SetUp();
}

consoleengine::ConsoleEngine::ConsoleEngine(int width, int height)
{
	MoveWindow(hwnd, 100, 100, width, height, TRUE);
	SetUp();
}

void consoleengine::ConsoleEngine::SetUp()
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO     cursorInfo;
	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = false; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
	lastTime = clock();
	entityManager = new EntityManager();
	drawConsole = new DrawConsole(entityManager);
}

Text * consoleengine::ConsoleEngine::CreateText(int x, int y, int z, string text, int colour)
{
	return entityManager->CreateText(x, y, z, text, colour);
}

Text * consoleengine::ConsoleEngine::CreateText(int x, int y, int z, string text, char invisibleChar, int colour)
{
	return entityManager->CreateText(x, y, z, text, invisibleChar, colour);
}

void consoleengine::ConsoleEngine::DeleteText(Text * text)
{
	entityManager->DeleteText(text);
	text = NULL;
}

void consoleengine::ConsoleEngine::Draw()
{
	drawConsole->Draw();
}

void consoleengine::ConsoleEngine::ExitEngine()
{
	//Unload renderer and objects
	running = false;
	//Unload and delete objects
	entityManager->~EntityManager();
	//Unload DrawConsole class
	drawConsole->~DrawConsole();
}

bool consoleengine::ConsoleEngine::Running()
{
	drawConsole->Draw();
	return running;
}

void consoleengine::ConsoleEngine::ForceDraw()
{
	drawConsole->Draw();
}

void consoleengine::ConsoleEngine::SetBounds(int width, int height)
{
	this->width = width;
	this->height = height;
	drawConsole->GiveConsoleSize(width, height);
}

int consoleengine::ConsoleEngine::GetBoundsWidth()
{
	return width;
}

int consoleengine::ConsoleEngine::GetBoundsHeight()
{
	return height;
}

float consoleengine::ConsoleEngine::Timer()
{
	clock_t time = clock();
	float duration = (time - lastTime) / (float)1000;
	lastTime = time;
	return duration;
}

char consoleengine::ConsoleEngine::GetInput()
{
	if (_kbhit() == 0) {
		return 0;
	}
	else {
		return (char)_getch();
	}
}

void consoleengine::ConsoleEngine::SetBackgroundColour(int colour)
{
	drawConsole->SetBackgroundColour(colour);
}