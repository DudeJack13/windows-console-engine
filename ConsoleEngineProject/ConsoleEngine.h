//---------------------------------
//Windows Console Engine v2.0
//-Jack Sylvester-Barnett
//--------------------------------
#ifndef CONSOLEENGINE_H
#define CONSOLEENGINE_H
#pragma once

#include "EntityManager.h"
#include "DrawConsole.h"

#include <ctime>
#include <conio.h>

namespace consoleengine {
	
	class ConsoleEngine
	{
	private:
		bool running = true;
		int width = 0;
		int height = 0;
		clock_t lastTime;
		EntityManager* entityManager = NULL;
		DrawConsole* drawConsole = NULL;
		HWND hwnd = GetConsoleWindow();

		void SetUp();
		void Draw();
	public:
		ConsoleEngine();
		ConsoleEngine(int width, int height);
		
		//Text functions
		Text * CreateText(int x, int y, int z, string text, int colour);
		Text * CreateText(int x, int y, int z, string text, char invisibleChar, int colour);
		void DeleteText(Text* text);

		//Draw functions
		bool Running();
		void ExitEngine();
		void ForceDraw();

		//Engine functions
		void SetBounds(int width, int height);
		int GetBoundsWidth();
		int GetBoundsHeight();
		void SetBackgroundColour(int colour);

		//Helper functions
		float Timer();
		char GetInput();
	};
}

#endif // !CONSOLEENGINE_H

