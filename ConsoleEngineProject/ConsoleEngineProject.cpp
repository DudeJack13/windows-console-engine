// ConsoleEngineProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConsoleEngine.h"

using namespace consoleengine;

int main()
{
	//---------------------------
	//For testing
	//---------------------------
	ConsoleEngine* cE = new ConsoleEngine(400, 800);
	cE->SetBounds(20, 20);
	cE->SetBackgroundColour(87);
	vector <Text*> objects = {
		cE->CreateText(10,10,5,"[][][][]", 73),
		cE->CreateText(4, 4, 3, "123**\n1234*\n12345\n1234*", '*', 160),
		cE->CreateText(5, 10, 3, "[]**\n[][]\n**[]", '*', 68)
	};
	Text* text = cE->CreateText(1, 1, 0, 
		to_string(objects.front()->GetHeight()) + "|" + to_string(objects.front()->GetWidth()), 57);

	Text* testObj = NULL;

	while (cE->Running()) {

		cE->Timer();

		testObj = cE->CreateText(1, 1, 1, "@", '*', 15);
		cE->DeleteText(testObj);
		testObj = NULL;

		char in = cE->GetInput();
		if (in == 'w') { //W
			objects.front()->MoveUp(1);
			text->SetColour(63);
		}
		else if (in == 's') { //S
			objects.front()->MoveDown(1);
			text->SetColour(163);
		}
		else if (in == 'd') { //D
			objects.front()->MoveRight(1);
		}
		else if (in == 'a') { //A
			objects.front()->MoveLeft(1);
		}
		else if (in == 'q') { //Q
			cE->DeleteText(objects.front());
			objects.erase(objects.begin());
		}
		else if (in == 't') { //A
			objects.front()->Visible(false);
		}
		else if (in == 27) { //Escape
			cE->ExitEngine();
		}

		if (objects.front()->Collision(objects.back())) {
			text->SetText("True");
		}
		else {
			text->SetText("False");
		}
	}
	return 0;
}

