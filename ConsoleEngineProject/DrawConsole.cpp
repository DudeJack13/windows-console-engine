#include "stdafx.h"
#include "DrawConsole.h"

DrawConsole::DrawConsole(EntityManager * entityManager)
{
	this->entityManager = entityManager;

	//Get console information
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	//Hide cursor
	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(hOut, &cursorInfo);
	cursorInfo.bVisible = false;
	SetConsoleCursorInfo(hOut, &cursorInfo);
}

DrawConsole::~DrawConsole()
{
	//Unload pixel buffer
	for (auto &yRow : pixels) {
		for (auto &x : yRow) {
			delete(x);
		}
		yRow.clear();
	}
}

DrawConsole::Pixel * DrawConsole::GetPixelInfo(int x, int y)
{
	return pixels[y][x];
}

void DrawConsole::Draw()
{
	//Remove deleted objects
	RemoveDeletedText();
	//Draw the text
	DrawTextToConsole();

	//----------------------------------------------
	//Draw Buffer to console
	//----------------------------------------------
	string out = "";
	for (auto &yRow : pixels) {
		for (auto &x : yRow) {
			SetColour(x->colour);
			cout << x->character;
		}
		cout << "\n";
	}
}

void DrawConsole::RemoveDeletedText()
{
	while (!entityManager->IfDeletionEmpty()) {
		Text* temp = entityManager->GetDeletedText();
		ClearText(temp, true);
		entityManager->RemoveDeletedText();
	}
}

void DrawConsole::DrawTextToConsole()
{
	//Reset cursor at top left for each frame
	SetCursorPosition(0, 0);
	//----------------------------------------------
	//Clear the objects
	//----------------------------------------------
	for (auto &obj : entityManager->GetTextList()) {
		ClearText(obj, false);
	}

	//----------------------------------------------
	//Redraw the objects in buffer (All ojects due to overlaping)
	//----------------------------------------------
	for (auto &text : entityManager->GetTextList()) {
		//Draw object to console
		if (text->IfVisible()) {
			int yLine = text->GetY();
			int xLine = text->GetX();
			for (auto &line : text->GetCharacter()) {
				if (InBounds(xLine + line->x, yLine + line->y)) {
					GetPixelInfo(xLine + line->x, yLine + line->y)->character = line->character;
					GetPixelInfo(xLine + line->x, yLine + line->y)->colour = line->colour;
				}
			}
		}
	}
}

void DrawConsole::ClearText(Text * text, bool forceDeletion)
{
	//Get object information
	int lX = text->GetLastFrameX();
	int lY = text->GetLastFrameY();
	//Clear object from buffer if moved
	if (text->GetX() != lX || text->GetY() != lY || forceDeletion || !text->IfVisible() || text->IfTextChanged()) {
		for (auto &c : text->GetBeforeCharacters()) {
			if (InBounds(lX + c->x, lY + c->y)) {
				GetPixelInfo(lX + c->x, lY + c->y)->colour = defaultColour;
				GetPixelInfo(lX + c->x, lY + c->y)->character = ' ';
			}
		}
	}
	text->UpdateLastFrameLocation();
}

bool DrawConsole::InBounds(int x, int y)
{
	bool a = x > -1 && x < consoleWidth && y > -1 && y < consoleHeight;
	return a;
}

void DrawConsole::GiveConsoleSize(int width, int height)
{
	consoleHeight = height;
	consoleWidth = width;

	vector <Pixel*> tempX;
	for (int y = 0; y < consoleHeight; y++) {
		for (int x = 0; x < consoleWidth; x++) {
			tempX.push_back(new Pixel());
		}
		pixels.push_back(tempX);
		tempX.clear();
	}
}

void DrawConsole::SetColour(int colour)
{
	cout.flush();
	SetConsoleTextAttribute(hOut, colour);
}

void DrawConsole::SetCursorPosition(int x, int y)
{
	cout.flush();
	COORD coord = { (SHORT)x, (SHORT)y };
	SetConsoleCursorPosition(hOut, coord);
}

void DrawConsole::SetBackgroundColour(int colour)
{
	string out = "COLOR " + to_string(colour);
	system(out.c_str());
}

