#ifndef DRAWCONSOLE_H
#define DRAWCONSOLE_H
#pragma once

#include <windows.h>
#include <iostream>

#include "EntityManager.h"

class DrawConsole
{
private:
	EntityManager * entityManager;

	struct Pixel
	{
		int colour = 15;
		char character = ' ';
	};
	vector <vector<Pixel*>> pixels;

	Pixel* GetPixelInfo(int x, int y);

	HANDLE hOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	void SetColour(int colour);
	void SetCursorPosition(int x, int y);
	bool InBounds(int x, int y);
	void DrawTextToConsole();
	void RemoveDeletedText();
	void ClearText(Text* obj, bool forceDeletion);

	const int defaultColour = 015; //or 007
	int consoleHeight = 0;
	int consoleWidth = 0;

public:
	DrawConsole() {}
	DrawConsole(EntityManager* entityManager);
	~DrawConsole();

	void Draw();
	void GiveConsoleSize(int width, int height);
	void SetBackgroundColour(int colour);
};

#endif // !DRAWCONSOLE_H