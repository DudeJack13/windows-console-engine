#include "stdafx.h"
#include "EntityManager.h"


EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{
	while (!textList.empty()) {
		textList.back()->~Text();
		delete(textList.back());
		textList.pop_back();
	}
}

Text * EntityManager::CreateText(int x, int y, int z, string text, int colour)
{
	return CreateText(x, y, z, text, '`', colour);
}

Text * EntityManager::CreateText(int x, int y, int z, string text, char invisibleCharacter, int colour)
{
	for (size_t a = 0; a < textList.size(); a++) {
		if (z <= textList[a]->GetZ()) {
			Text* temp = new Text(x, y, z, text, invisibleCharacter,colour);
			textList.insert(textList.begin() + a, temp);
			return temp;
		}
	}
	textList.push_back(new Text(x, y, z, text, invisibleCharacter, colour));
	return textList.back();
}

void EntityManager::DeleteText(Text * text)
{
	for (size_t a = 0; a < textList.size(); a++) {
		if (text == textList[a]) {
			text->~Text();
			delete(textList[a]);
			textList.erase(textList.begin() + a);
			break;
		}
	}
}

vector<Text*> EntityManager::GetTextList()
{
	return textList;
}

bool EntityManager::IfDeletionEmpty()
{
	return deletionList.empty();
}

Text * EntityManager::GetDeletedText()
{
	return deletionList.back();
}

void EntityManager::RemoveDeletedText()
{
	deletionList.back()->~Text();
	delete(deletionList.back());
	deletionList.pop_back();
}