#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H
#pragma once

#include "Text.h"

class EntityManager
{
private:
	vector<Text*> textList;
	vector<Text*> deletionList;
public:
	EntityManager();
	~EntityManager();

	//Text functions
	Text * CreateText(int x, int y, int z, string text, int colour);
	Text * CreateText(int x, int y, int z, string text, char invisibleCharacter, int colour);
	void DeleteText(Text* text);

	Text* GetDeletedText();
	void RemoveDeletedText();
	vector <Text*> GetTextList();
	bool IfDeletionEmpty();
};

#endif // ENTITYMANAGER_H