#include "stdafx.h"
#include "ObjectFunctions.h"

void ObjectFunctions::SetCharacters()
{
	while (!characters.empty()) {
		characters.pop_back();
	}

	height = 0;
	width = 0;
	int xCounter = 0;
	int yCounter = 0;
	for (auto &letter : this->text) {
		if (letter == '\n') {
			height++;
			yCounter++;
			if (xCounter > width) {
				width = xCounter;
			}
			xCounter = 0;
		}
		else if (letter != invisibleChar){
			characters.push_back(unique_ptr<Character>(new Character(letter, colour, xCounter, yCounter)));
			xCounter++;
		}
	}
	if (xCounter > 0) {
		width = xCounter;
	}
}

void ObjectFunctions::SetBeforeCharacters()
{
	beforeCharacters.clear();
	beforeCharacters = move(characters);
	SetCharacters();
}

//------------------------
//POSITION GETTER
//------------------------
int ObjectFunctions::GetX()
{
	return x;
}

int ObjectFunctions::GetY()
{
	return y;
}

int ObjectFunctions::GetZ()
{
	return z;
}

int ObjectFunctions::GetLastFrameX()
{
	return xB;
}

int ObjectFunctions::GetLastFrameY()
{
	return yB;
}

void ObjectFunctions::UpdateLastFrameLocation()
{
	yB = y;
	xB = x;
	SetBeforeCharacters();
}

//------------------------
//COLOUR FUNCTION
//------------------------
void ObjectFunctions::SetColour(int colour)
{
	this->colour = colour;
}

int ObjectFunctions::GetColour()
{
	return colour;
}

//------------------------
//MOVE FUNCTION
//------------------------
void ObjectFunctions::MoveUp(int amount)
{
	y -= amount;
}

void ObjectFunctions::MoveDown(int amount)
{
	y += amount;
}

void ObjectFunctions::MoveLeft(int amount)
{
	x -= amount;
}

void ObjectFunctions::MoveRight(int amount)
{
	x += amount;
}

void ObjectFunctions::SetPosition(int x, int y)
{
	this->x = xB;
	this->y = yB;
	this->x = x;
	this->y = y;
}

int ObjectFunctions::GetHeight()
{
	return height;
}

int ObjectFunctions::GetWidth()
{
	return width;
}

void ObjectFunctions::SetText(string newText)
{
	lastTxt = text;
	SetBeforeCharacters();
	text = newText;
	SetCharacters();
}

void ObjectFunctions::SetInvisibleChar(char newchar)
{
	this->invisibleChar = newchar;
}

void ObjectFunctions::Visible(bool visible)
{
	this->visible = visible;
}

bool ObjectFunctions::IfVisible()
{
	return visible;
}

vector<unique_ptr<ObjectFunctions::Character>> ObjectFunctions::GetCharacter()
{
	vector<unique_ptr<Character>> temp;
	for (auto &c : characters) {
		temp.push_back(unique_ptr<Character>(new Character(
			c.get()->character, c.get()->colour, c.get()->x, c.get()->y)));
	}
	return move(temp);
}

vector<unique_ptr<ObjectFunctions::Character>> ObjectFunctions::GetBeforeCharacters()
{
	vector<unique_ptr<Character>> temp;
	for (auto &c : beforeCharacters) {
		temp.push_back(unique_ptr<Character>(new Character(
			c.get()->character, c.get()->colour, c.get()->x, c.get()->y)));
	}
	return move(temp);
}

bool ObjectFunctions::IfTextChanged()
{
	return text != lastTxt;
}

