#pragma once
#ifndef OBJECTFUNCTIONS_H
#define OBJECTFUNCTIONS_H

#include <vector>
#include <string>
#include <memory>

using namespace std;

class ObjectFunctions
{
protected:
	struct Character
	{
		Character() {}
		Character(char character, int colour, int x, int y) {
			this->x = x;
			this->y = y;
			this->colour = colour;
			this->character = character;
		}
		~Character() {}
		char character;
		int colour;
		int x;
		int y;
	};
	vector<unique_ptr<Character>> characters; //Holds each characters x,y and colour
	vector<unique_ptr<Character>> beforeCharacters;
	string text = "";
	string lastTxt = "";
	//Position
	int x;
	int xB;
	int y;
	int yB;
	int z;
	int colour;
	const int defaultColour = 15;
	char invisibleChar;
	bool visible = true;
	int width;
	int height = 1;
	void SetCharacters();
public:
	//Position getters
	int GetX();
	int GetY();
	int GetZ();

	void UpdateLastFrameLocation();

	//Position Function
	void MoveUp(int amount);
	void MoveDown(int amount);
	void MoveLeft(int amount);
	void MoveRight(int amount);
	void SetPosition(int x, int y);

	//Properties
	int GetHeight();
	int GetWidth();
	void SetText(string newText);
	void SetInvisibleChar(char newchar);


	//Colour
	void SetColour(int colour);
	int GetColour();

	//Other
	void Visible(bool visible);
	bool IfVisible();

	vector<unique_ptr<Character>> GetCharacter();
	vector<unique_ptr<Character>> GetBeforeCharacters();

	void SetBeforeCharacters();

	//Rendering
	int GetLastFrameX();
	int GetLastFrameY();
	bool IfTextChanged();
};

#endif // !OBJECTFUNCTIONS_H