#include "stdafx.h"
#include "Text.h"

Text::Text()
{
	x = 0;
	y = 0;
	text = "";
	colour = 15;
}

Text::Text(int x, int y, int z, string text, char invisbileChar, int colour)
{
	SetUp(x, y, z, text, invisbileChar, colour);
}

void Text::SetUp(int x, int y, int z, string text, char invisibleChar, int colour)
{
	this->x = x;
	this->xB = x;
	this->y = y;
	this->yB = y;
	this->z = z;
	this->text = text;
	this->invisibleChar = invisibleChar;
	this->colour = colour;

	SetCharacters();

	if (this->text.back() != '\n') {
		this->text += '\n';
	}
}

bool Text::Collision(Text * object)
{
	if (y + height > object->GetY() && //Top
		y < object->GetY() + object->GetHeight() && //Bottom
		x + width > object->GetX() && //Left
		x < object->GetX() + object->GetWidth()) { //Right

		for (auto &l1 : characters) {
			for (auto &l2 : object->GetCharacter()) {
				if (l1->x + x == l2->x + object->GetX() &&
					l1->y + y == l2->y + object->GetY()) {
					return true;
				}
			}
		}
	}
	return false;
}