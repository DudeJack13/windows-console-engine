#pragma once
#ifndef TEXT_H
#define TEXT_H

#include "ObjectFunctions.h"

class Text : public ObjectFunctions
{
private:
	void SetUp(int x, int y, int z, string text, char ic, int colour);
	
public:
	Text();
	Text(int x, int y, int z, string text, char invisbileChar, int colour);
	~Text() {}
	bool Collision(Text* object2);
};

#endif // !TEXT_H