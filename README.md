# Windows Console Engine 2.0 Documentation  #
## Jack Sylvester-Barnett ##

## Summary ##
The console engine is designed to work on Window Command Prompt terminal. The engine deals with any objects that have been created within the engine and draws them onto the console. The engine can move an object around the console on the X, Y and Z axis, the Z axis is layering for drawing the screen. 

## ConsoleEngine ##
Main class that user interacts with, everything can be controlled from here. From this class, objects can be created and deleted, the console size and bounds are set and the update function is called. 

## DrawConsole ##
Draw console handles everything that gets drawn to the console itself. It rewrites over objects that exist instead of clearing the while screen and re-writing. The main public `Draw` is used to draw the updated objects to the console screen. The `Draw` function requires and list of objects that need drawing sorted by their `z` value, higher the `z` value, the further forward they are draw. Each loop, any existing objects that have moved location from the last frame is cleared from the console and re-drawn in its new location. 